### Professional
* **Research Assistant** [Jan 2021 - Present]  
  Center for Wireless, Communities and Innovation ([WiCI](https://wici.iastate.edu/))  
  Iowa State University ([ISU](https://www.iastate.edu/)), Ames Iowa

* **Wireless Engineering Intern** [May 2023 - August 2023]  
  [Skylark Wireless, LLC](https://skylarkwireless.com/), Houston Texas 

* **Assistant Divisional Engineer** [Jul 2015 - December 2020]  
  National Telecomm Corporation ([NTC](https://ntc.net.pk/)), Islamabad Pakistan

* **Application Support Engineer** [August 2014 - July 2015]        
  [Communicator's Globe (Pvt.) Ltd](https://communicatorsglobe.com/), Islamabad Pakistan

* **Intern** [Jun 2014 - Aug 2014]
  [Mobilink Jazz](https://jazz.com.pk/), Islamabad Pakistan

### Education
* **Doctor of Philosophy (Ph.D.)** in *Computer Engineering* [Jan 2021 - Dec 2025]  
  Iowa State University ([ISU](https://www.iastate.edu/)), Ames Iowa  
  <!-- *Thesis*: Ph.D. thesis title ( [<img src="images/pdf.png" height=14>
  PDF](link_to_the_thesis_pdf) )  
  *Advisor*: [Hongwei Zhang](link_to_advisor_web_page)   -->


* **Master's Degree** in *Electrical Engineering* [Aug 2016 - Mar 2020]  
  National University of Sciences and Technology ([NUST](https://nust.edu.pk/))  
  <!-- *Thesis*: Master's thesis title ( [<img src="images/pdf.png" height=14>
  PDF](link_to_the_thesis_pdf) )  
  *Advisor*: [Advisor Name](link_to_advisor_web_page)   -->

* **Bachelor's Degree** in *Electrical Engineering* [Aug 2010 - June 2014]  
  National University of Sciences and Technology ([NUST](https://nust.edu.pk/))


<!-- ### Awards | Scholarships | Certificates

* Award 1 [Month Year]
* Award 2 [Month Year]
* Award 3 [Month Year] -->
