#!/usr/bin/python3

import sys
import markdown2 as mk


def main():
    filename = sys.argv[1]

    level_count = filename.count('/')

    # Read markdown data
    with open('%s.md' % (filename)) as fp_in:
        md_data = fp_in.read()

    # Convert markdown to html
    html_data = mk.markdown(md_data)

    fp_out = open('%s.html' % (filename), 'w')
    # Create header
    content = '<head>\n<link rel="stylesheet" href="%smystyle.css">\n<base target="_blank">\n' % (
        '../'*level_count)
    content += '</head>\n'
    fp_out.write(content)

    with open('title.dat') as fp_in:
        content = fp_in.read()

    fp_out.write('<font size="5" color="#c12f2f"><b>%s</b></font>' % (content))
    fp_out.write('<div class="headlink">\n')
    with open('navigation.dat') as fp_in:
        for line in fp_in:
            comps = line.strip().split(':')
            content = '<a href="%s%s" target="_self">%s</a> \n' % ('../'*level_count,
                                                                   comps[1], comps[0])
            fp_out.write(content)

    fp_out.write('</div>\n')
    fp_out.write(html_data)

    content = f'<center>{4*"&emsp;&#9643;"}</center>'
    fp_out.write(content)
    fp_out.close()


if __name__ == '__main__':
    main()
