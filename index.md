### About Me


<img src="images/Taimoor_HS.jpg" width="140" hspace="10pt"
style="float:left"> 

I am a Ph.D. student of Computer Engineering and my current research explores practical problems in wireless
and networked systems. I have over a decade of experience working on the design, deployment, operation,
optimization, and provenance of communication and networked systems with six years of hands-on industrial
service. Having honed my skills in problem-solving, critical-thinking, communication and leadership during
all these years, I am eager to channel my expertise into leadership roles on projects of national significance,
with the ambition of driving community transformation and making a substantial, far-reaching impact.

<br>

[<img src="images/dblp.png" height=25>](https://dblp.org "dblp")
[<img src="images/googlescholar.png"
height=20>](https://scholar.google.co.in "Google Scholar")
[<img src="images/github.png"
height=22>](https://github.com "GitHub")
[<img
src="images/webofscience.png"
height=22>](http://www.webofscience.com "Web of Science")
[<img src="images/orcid.png" height=22>](https://orcid.org "Orcid")<br>
[<img
src="images/linkedin.png"
height=22>](https://www.linkedin.com "LinkedIn")
[<img src="images/gitlab.png"
height=24>](https://gitlab.com "GitLab")
[<img src="images/researchgate.png"
height=22>](https://www.researchgate.net "ResearchGate")
[<img
src="images/kudos.png"
height=22>](https://www.growkudos.com "Kudos")

