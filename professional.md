### Affiliations

* Member, [Society 1](URL_to_society_1) [Start Date - End Date]
* Member, [Society 2](URL_to_society_2) [Start Date - End Date]
  
### Technical Program Committees (TPCs)

* [Conference 1](URL_to_Conference_1)
* [Conference 2](URL_to_Conference_2)


### As Reviewer

* **Journals**
	* [Journal 1](URL_to_Journal_1)
	* [Journal 2](URL_to_Journal_2)
