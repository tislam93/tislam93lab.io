
### Refereed International Journals

1. Author 1, **Author 2**, and Author 3, "[*Title of the Article
   1*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.
3. Author 1, **Author 2**, and Author 3, "[*Title of the Article
   2*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.
2. Author 1, **Author 2**, and Author 3, "[*Title of the article
   3*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.
1. Author 1, **Author 2**, and Author 3, "[*Title of the article
   4*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.

### Refereed International Conferences

1. Author 1, **Author 2**, and Author 3, "[*Title of the Article
   1*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.
3. Author 1, **Author 2**, and Author 3, "[*Title of the Article
   2*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.
2. Author 1, **Author 2**, and Author 3, "[*Title of the article
   3*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.
1. Author 1, **Author 2**, and Author 3, "[*Title of the article
   4*](DOI_Link_to_the_article)," ****, vol. 1, pp. 1-6, Mon. Year.

### Book Chapters

1. Author 1, **Author 2**, and Author 3, "[*Book Chapter
   Title*](DOI_Link_to_the_Article), Publisher,
   2nd Edition, YEAR, vol. 3, pp. 1–16. Publisher.

   
### arXiv/TechRxiv Preprints


1. Author 1, **Author 2**, and Author 3, "[*Title of the article
   1*](DOI_Link_to_the_article)," **Preprint ID**, Mon. Year.
2. Author 1, **Author 2**, and Author 3, "[*Title of the article
   2*](DOI_Link_to_the_article)," **Preprint ID**, Mon. Year.
1. Author 1, **Author 2**, and Author 3, "[*Title of the article
   3*](DOI_Link_to_the_article)," **Preprint ID**, Mon. Year.



### Technical Reports

1. Author 1, **Author 2**, and Author 3, "[*Technical Report
   Title*]()," Venue, Tech. Rep., Mon Year, Tech-Report-ID.


