### Research Focus & Interests

* **NextG Wireless Research Platforms:** Develop, optimize, and implement large-scale, real-world programmable wireless testbeds for advancing next-generation wireless networks and enabling co-evolution of wireless communication technologies and applications, with real-world impacts such as affordable rural broadband connectivity, enabling ultra-reliable low-latency communication, and fostering innovations in 5G/6G and Open RAN systems.

* **mMIMO URLLC:** Enabling ultra-reliable low-latency communications with multi-cell mMIMO cellular networks. 

* **Rural Broadband:** Addressing the digital divide with rural-focussed research such as TVWS mMIMO and intelligent interference control for efficient use of spectrum, reducing costs and enabling affordable rural broadband connectivity.


### Research Projects Involved

1. **ARA Wireless Living Lab** [Jun 2021 - May 2025]: Leading wireless network subgroup in [ARA PAWR Project](https://arawireless.org/), a $16M wireless living lab funded by NSF for addressing the rural broadband challenges.

2. **ACCoRD** [Mar 2024 - Mar 2027]: Lead member of $42M Acceleration of Compatibility and Commercialization for Open RAN Deployments [(ACCoRD)](https://www.ntia.gov/press-release/2024/biden-harris-administration-awards-42m-wireless-innovation) project, establishing testing centers for Open RAN wireless innovation. 

<!-- 2. **Name of Project 2** [Start Date - End Date]: Short description of
   Project 2. 

1. **Name of Project 1** [Start Date - End Date]: Short description of
   Project 1.  -->
